			                            OpenCDISC Open Source Software License
						
By exercising the Licensed Rights (defined below), You accept and agree to be bound by the terms and conditions of this
OpenCDISC Open Source Software License ("License Agreement"). You are granted the Licensed Rights in consideration of
Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the
Licensor receives from making the Licensed Material available under these terms and conditions.

Section 1 – Definitions.
		
	a. Adapted Material means material subject to Copyright and Similar Rights   that is derived from or based upon the
	Licensed Material and in which the Licensed Material is translated, altered, arranged, transformed, or otherwise
	modified in a manner requiring permission under the Copyright and Similar Rights held by the Licensor. In addition,
	and without limiting the foregoing, any software created by You or on Your behalf that integrates with the Licensed
	Material (including, without limitation, by any public or other interface, sharing of the same computer memory space,
	dynamically or statically linking, execution within the same classpath or java virtual machine or automated execution
	via a separate process) will be deemed to be Adapted Material.

	b. Copyright and Similar Rights means copyright and/or similar rights closely related to copyright including, without
	limitation, performance, broadcast, sound recording, and Sui Generis Database Rights, without regard to how the rights
	are labeled or categorized. For purposes of this License Agreement, the rights specified in Section 2(b)(1)-(2) are
	not Copyright and Similar Rights.

	c. Commercial Use means charging a fee, royalty, or any other form of consideration for any products or services
	derived from or based upon the Licensed Material or that use, reference or integrate with the Licensed Material
	(including, without limitation, by any public or other interface, sharing of the same computer memory space,
	dynamically or statically linking, execution within the same classpath or java virtual machine or automated execution
	via a separate process).

	d. Covered Materials means Licensed Material or Adapted Material, combined or separately, and any part thereof.
	
	e. Effective Technological Measures means those measures that, in the absence of proper authority, may not be
	circumvented under laws fulfilling obligations under Article 11 of the WIPO Copyright Treaty adopted on
	December 20, 1996, and/or similar international agreements.

	f. Licensed Material means the OpenCDISC Validator, Define.xml Generator, Data Converter, and ClinicalTrials.gov Miner,
	in object and source code form, and any related tools, files, documentations, validation rules and/or material
	(including, but not limited to, Licensor’s customized version of the SDTM, SEND, ADaM, and Define.xml rules),
	made available to You by Licensor.

	g. Licensed Rights means the rights granted to You subject to the terms and conditions of this License Agreement,
	which are limited to all Copyright and Similar Rights that apply to Your use of the Licensed Material and Adapted
	Material and that the Licensor has authority to license.

	h. Licensor means Pinnacle 21 LLC.

	i. Non-Commercial Use means any personal use other than a Commercial Use.

	j. Share means to provide material to others by any means or process that requires permission under Copyright and
	Similar Rights or the Licensed Rights, such as reproduction, public display, public performance, distribution,
	dissemination, communication, or importation, and to make material available to the public including in ways that
	members of the public may access the material from a place and at a time individually chosen by them.

	k. Sui Generis Database Rights means rights other than copyright resulting from Directive 96/9/EC of the European
	Parliament and of the Council of 11 March 1996 on the legal protection of databases, as amended and/or succeeded,
	as well as other essentially equivalent rights anywhere in the world.

	l. You means the individual or entity exercising the Licensed Rights under this License Agreement. Your has a
	corresponding meaning.

Section 2 – Scope.

	a. License grant. 

		1. Subject to the terms and conditions of this License Agreement (including Section 2(a)(2) below), the Licensor
		hereby grants You a worldwide, royalty-free, non-sublicensable, non-exclusive license to exercise the Licensed
		Rights in the Licensed Material to:

			A. reproduce and use the Licensed Material, in whole or in part, solely for Your own Non-Commercial Use; 
				
			B. use the Licensed Material solely in connection with consulting or other professional services provided by
			You to Your customers, provided that You never permit your customers to directly or indirectly use the
			Covered Materials as or in conjunction with any software or technology product or service (including,
			without limitation, any software-as-a-service or platform-as-a-service offering), and provided further that
			You never charge a fee to Your customers for the Licensed Material;
				
			C. create, reproduce, and use Adapted Material solely for Your own Non-Commercial Use; and
				
			D. share the Licensed Material, but not any Adapted Material.

		2. Exceptions and Limitations. As a condition of granting you the Licensed Rights, you shall not:

			A. sell, rent, loan license or sublicense the Covered Materials or otherwise market, share or use the
			Covered Materials for any Commercial Use;
				
			B. Share any Adapted Material;
				
			C. directly or indirectly market, Share, or offer to any other person the Covered Materials as or in
			conjunction with any software or technology product or service (including, without limitation, any
			software-as-a-service or platform-as-a-service offering);
				
			D. use or automate the use of the Covered Materials as part of or in conjunction with any software or
			technology product or service (including, without limitation, any software-as-a-service or
			platform-as-a-service offering); and
				
			E. market, Share or offer the Covered Materials as or in conjunction with any time-sharing or service-bureau
			product or service.
			
		3. Attribution. If you make any reproduction or copy of the Covered Materials or Share the Licensed Material,
		You must include the same and maintain the prominence of the copyright notices, references to this
		License Agreement, and identification and branding of the Licensor as contained in the original copy of the
		Licensed Material.
			
		4. Term. The term of this License Agreement is specified in Section 5(a).
			
		5. Media and formats; technical modifications allowed. The Licensor authorizes You to exercise the
		Licensed Rights in all media and formats whether now known or hereafter created, and to make technical
		modifications necessary to do so. For purposes of this License Agreement, simply making modifications authorized
		by this Section 2(a)(5) never produces Adapted Material.
			
		6. No endorsement. Nothing in this License Agreement constitutes or may be construed as permission to assert or
		imply that You are, or that Your use of the Licensed Material is, connected with, or sponsored, endorsed, or
		granted official status by, the Licensor or others.
			
		7. Commercial Use. The Licensed Rights are limited to Your Non-Commercial Use of the Covered Materials. You may
		only use the Covered Materials for Commercial Use if that use is pursuant to a separately negotiated commercial
		license agreement entered into between You and the Licensor.
			
		8. Licensor reserves all rights not expressly granted in this License Agreement.
		
	b. Other rights.

		1. Moral rights, such as the right of integrity, are not licensed under this License Agreement, nor are publicity,
		privacy, and/or other similar personality rights; however, to the extent possible, the Licensor waives and/or
		agrees not to assert any such rights held by the Licensor to the limited extent necessary to allow You to exercise
		the Licensed Rights in accordance with this License Agreement, but not otherwise.
			
		2. Patent and trademark rights are not licensed under this License Agreement.
			
		3. Subject to your compliance with this License Agreement, to the extent possible, the Licensor waives any right
		to collect royalties from You for the exercise of the Licensed Rights, whether directly or through a collecting
		society under any voluntary or waivable statutory or compulsory licensing scheme. In all other cases, subject to
		your compliance with this License Agreement, the Licensor expressly reserves any right to collect such royalties.

Section 3 – Sui Generis Database Rights.

Where the Licensed Rights include Sui Generis Database Rights that apply to Your use of the Licensed Material:

	a. for the avoidance of doubt, Section 2(a)(1) grants You the right to extract, reuse, and reproduce all or a
	substantial portion of the contents of the database, provided Your extraction, reuse, and reproduction is solely for
	Your own Non-Commercial Use, not for any purposes prohibited by Section 2(a)(2) and otherwise in compliance with
	this License Agreement; and
		
	b. if You include all or a substantial portion of the database contents in a database in which You have Sui Generis
	Database Rights, then the database in which You have Sui Generis Database Rights (but not its individual contents)
	is Adapted Material.
		
For the avoidance of doubt, this Section 3 supplements and does not replace Your obligations under this License Agreement
where the Licensed Rights include other Copyright and Similar Rights.

Section 4 – Disclaimer of Warranties and Limitation of Liability.
	
	a. Unless otherwise separately undertaken by the Licensor in writing, to the extent possible, the Licensor offers
	the Licensed Material as-is and as-available, and makes no representations or warranties of any kind concerning the
	Licensed Material, whether express, implied, statutory, or other. This includes, without limitation, warranties of
	title, merchantability, fitness for a particular purpose, non-infringement, absence of latent or other defects,
	accuracy, or the presence or absence of errors, whether or not known or discoverable. Where disclaimers of warranties
	are not allowed in full or in part, this disclaimer may not apply to You.
		
	b. To the extent possible, in no event will the Licensor be liable to You on any legal theory (including, without
	limitation, negligence) or otherwise for any direct, special, indirect, incidental, consequential, punitive,
	exemplary, or other losses, costs, expenses, or damages arising out of this License Agreement or use of the Licensed
	Material, even if the Licensor has been advised of the possibility of such losses, costs, expenses, or damages.
	Where a limitation of liability is not allowed in full or in part, this limitation may not apply to You.
		
	c. The disclaimer of warranties and limitation of liability provided above shall be interpreted in a manner that,
	to the extent possible, most closely approximates an absolute disclaimer and waiver of all liability.

Section 5 – Term and Termination.

	a. This License Agreement applies for the term of the Copyright and Similar Rights licensed here. However, if You
	fail to comply with this License Agreement, then Your rights under this License Agreement terminate automatically.
		
	b. Where Your right to use the Licensed Material has terminated under Section 5(a), it reinstates:
			
		1. automatically as of the date the violation is cured, provided it is cured within 30 days of Your discovery of
		the violation; or
			
		2. upon express reinstatement by the Licensor.

For the avoidance of doubt, this Section 5(b) does not affect any right the Licensor may have to seek remedies for Your
violations of this License Agreement.

	c. For the avoidance of doubt, the Licensor may also offer the Licensed Material under separate terms or conditions
	or stop distributing the Licensed Material at any time; however, doing so will not terminate this License Agreement.

	d. Sections 1, 4, 5, 6, and 7 survive termination of this License Agreement.

Section 6 – Other Terms and Conditions.

	a. The Licensor shall not be bound by any additional or different terms or conditions communicated by You unless
	expressly agreed in writing.
		
	b. Any arrangements, understandings, or agreements regarding the Licensed Material not stated herein are separate
	from and independent of the terms and conditions of this License Agreement.
		
	c. The laws of the Commonwealth of Pennsylvania shall govern the validity, construction, and performance of this
	Agreement, without giving effect to the conflict of laws rules thereof to the extent that the application of the
	laws of another jurisdiction would be required thereby.  You consent to the sole and exclusive jurisdiction of the
	federal and state courts located in or embracing Montgomery County, PA in all actions arising under this License
	Agreement and hereby irrevocably waive any claim that any such court lacks jurisdiction or that such venue or forum
	is inconvenient. In the event of an action or proceeding by Licensor to enforce or exercise its rights under this
	License Agreement, the Licensor shall be entitled to be reimbursed for its reasonable attorneys’, other
	professionals’, and experts’ fees and out-of-pocket legal costs.
		
	d. You agree that any breach of your obligations or representations under this License Agreement would result in
	irreparable injury to the Licensor, and that in the event of any breach or threatened breach hereof, the Licensor
	will be entitled to seek injunctive relief in addition to any other remedies to which it may be entitled, without
	the necessity of posting bond.

Section 7 – Interpretation.

	a. For the avoidance of doubt, this License Agreement does not, and shall not be interpreted to, reduce, limit,
	restrict, or impose conditions on any use of the Licensed Material that could lawfully be made without permission
	under this License Agreement.
		
	b. To the extent possible, if any provision of this License Agreement is deemed unenforceable, it shall be
	automatically reformed to the minimum extent necessary to make it enforceable. If the provision cannot be reformed,
	it shall be severed from this License Agreement without affecting the enforceability of the remaining terms and
	conditions.
		
	c. No term or condition of this License Agreement will be waived and no failure to comply consented to unless
	expressly agreed to by the Licensor.
		
	d. Nothing in this License Agreement constitutes or may be interpreted as a limitation upon, or waiver of, any
	privileges and immunities that apply to the Licensor or You, including from the legal processes of any jurisdiction
	or authority.

The following is not part of the License Agreement:

OpenCDISC Open Source Software License Copyright Pinnacle 21 LLC. The OpenCDISC Open Source Software License was adapted
from the Creative Commons Attribution-NoDerivatives 4.0 International License, which was obtained from the Creative
Commons Corporation.  The original, unmodified version the license agreement can be found at
[http://creativecommons.org/licenses/by-nd/4.0/].

