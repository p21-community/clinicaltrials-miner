/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import org.apache.commons.lang3.StringUtils;

import java.net.URL;

//http://clinicaltrials.gov/

public class SearchDetails {

    protected URL searchURL;

    //make into HashMap or Enum
    public final static int PHASE_0_CODE = 4;
    public final static int PHASE_1_CODE = 0;
    public final static int PHASE_2_CODE = 1;
    public final static int PHASE_3_CODE = 2;
    public final static int PHASE_4_CODE = 3;

    //http://clinicaltrials.gov/ct2/resources/download#DownloadMultipleRecords
    public final static String URL_SUFFIX = "&studyxml=true";   //protocol records only (no results)
    public final static String URL_SUFFIX2 = "&resultsxml=true"; //both the protocol and study results XML
    public final static String URL_SUFFIX3 = "&displayxml=true"; //display the search results list in XML in your browser - http://clinicaltrials.gov/ct2/help/how-use-search-results

    //http://www.nlm.nih.gov/pubs/techbull/ja02/ja02_ctgov.html
    //http://clinicaltrials.gov/info/help
    //URL Encoding
    public final static String LEFT_BRACKET_CODE = "%5B";
    public final static String RIGHT_BRACKET_CODE = "%5D";
    public final static String DOUBLE_QUOTE_CODE = "%22";
    public final static String AMPERSAND = "&amp;";
    public final static String SPACE_CODE = "+";
    public final static String APOSTROPHE = "%27";


    //empty Constructor
    public SearchDetails() {
    }

    public URL getSearchURL() {
        return searchURL;
    }

    public String normalizeURL(String searchString) {

        return searchString.trim()
                .replaceAll(" +", " ")  //remove all extra spaces but 1 for brevity
                .replace(" ", SPACE_CODE)  //then replace the space with %20 or + - avoid HTTP response code: 400. http://stackoverflow.com/questions/1634271/url-encoding-the-space-character-or-20
                .replace("\n", ""); //replace line feed.  ;
    }

    public void showURL() {
        System.out.println(StringUtils.rightPad("CT.gov URL: ", 23) + searchURL.toString());
    }

}
