/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class SearchInput {
    public class InputRecord { //nested class member used in Map below

        public String dataPath;
        public String dataDirName;
        public String storageType;
        public String expectedTrials;
        public String searchString;
        public String categoryFileName;
        public File categoryFile;
        public String reportType;
    }

    //only one class member, the map which represents all SEARCH records in the XML or CSV file
    public List<InputRecord> searchList = new ArrayList<>();    //could use LinkedList. java 7 diamond operator allows you to forgoe the ArrayList<InputRecord> syntax

    //Finds record by a query and report type
    public List<InputRecord> getQueryList(String query, String reportType, File categoryFile, String filePath) throws Exception {
        InputRecord record = new InputRecord();
        String dateTime = Utils.getTimestamp();

        record.storageType = "file";
        record.dataPath = filePath;
        record.dataDirName = "Pinnacle 21 Community ClinicalTrials Miner Data-" + dateTime;
        record.expectedTrials = "4";
        record.searchString = query;
        record.reportType = reportType;

        if (record.reportType.equals("Outcomes")) {
            record.categoryFile = categoryFile; //current directory of running program + /config/ + categoryFileName
        }
        searchList.add(record);
        return searchList;
    }

    public List<InputRecord> getSearchList(File inputFile) throws Exception {
        if (inputFile.getName().split("\\.")[1].equalsIgnoreCase("CSV")) //parse CSV document and store records in a HashMap
        {
            CSVReader csvReader = new CSVReader(new FileReader(inputFile));
            List<String[]> csvRecords = csvReader.readAll();
            Integer i = 0;

            for (String[] csvRecord : csvRecords) {
                InputRecord record = new InputRecord();
                if (csvRecord[0].toUpperCase().equals("ACTIVE")) {
                } //skip header record
                else if (csvRecord[0].equals("1"))                         //collect the active records
                {
                    i++;
                    System.out.print("Search " + i + " Enabled");

                    record.storageType = csvRecord[1];  //csv column 2
                    record.dataPath = checkDataPath(csvRecord[2]); //csv column 3
                    record.dataDirName = csvRecord[3];  //csv column 4
                    record.categoryFileName = csvRecord[4];  //csv column 5
                    record.expectedTrials = csvRecord[5];  //csv column 6
                    record.searchString = csvRecord[6];  //csv column 7
                    record.reportType = csvRecord[7];  //csv column 8;

                    // FIXME: System.getPropert("user.dir") is unreliable, it returns the path of directory that the program is being executed in, not where it is located
                    record.categoryFile = new File(System.getProperty("user.dir") + "config/" + record.categoryFileName); //current directory of running program + /config/ + categoryFileName

                    if (record.categoryFile.exists() &&
                            !StringUtils.isBlank(record.categoryFileName)
                            ) {
                        record.categoryFile = Utils.sortCSV(record.categoryFile, 2); //create new file as "_sorted.csv"
                        System.out.println(". with CATEGORY FILE: " + record.categoryFile.getName());
                    } else {
                        record.categoryFile = new File("c:/x.bad"); //need to set it to a nonexistent file so OutcomeMeasure.java can skip it
                        System.out.println("");
                    }

                    searchList.add(record);

                } else {
                    i++;
                    System.out.println("Search " + i + " Disabled");
                }
            }
            csvReader.close();
            System.out.println(StringUtils.rightPad("=", 100, "="));
        }
        //---------------------------------------------------------------------------------
        else if (inputFile.getName().split("\\.")[1].equalsIgnoreCase("XML")) {
            NodeList nodeList = Utils.getNodeList(inputFile, "inputParams"); //represents all records in XML document
            for (int i = 0; i < nodeList.getLength(); i++) {
                InputRecord record = new InputRecord();
                if (((Element) nodeList.item(i)).getAttribute("active").equals("1")) {
                    record.storageType = ((Element) nodeList.item(i)).getAttribute("storageType");
                    record.dataPath = checkDataPath(((Element) nodeList.item(i)).getAttribute("dataPath"));
                    record.dataDirName = ((Element) nodeList.item(i)).getAttribute("dataDirName");
                    record.categoryFileName = ((Element) nodeList.item(i)).getAttribute("categoryFile");
                    record.expectedTrials = ((Element) nodeList.item(i)).getAttribute("expectedTrials");
                    record.searchString = ((Element) nodeList.item(i)).getAttribute("searchString");
                    record.categoryFile = new File(System.getProperty("user.dir") + "/config/" + record.categoryFileName);

                    if (!record.categoryFileName.equals(null) && record.categoryFile.exists()) {
                        record.categoryFile = Utils.sortCSV(record.categoryFile, 2);
                        System.out.println("Category File: " + record.categoryFile.getName());
                    }
                    searchList.add(record);
                } else {
                    System.out.println("Search " + i + " is Disabled");
                }
            }
        }
        //---------------------------------------------------------------------------------
        return searchList;
    }

    private String checkDataPath(String dataPath) {
        //dataPath is "" or null, assume working directory for downloading and parsing data.
        if (StringUtils.isBlank(dataPath)) {
            return System.getProperty("user.dir") + "/reports";
        }
        return dataPath; //original value
    }
}
