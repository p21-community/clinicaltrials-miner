package org.opencdisc.clinicaltrialsminer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.opencdisc.clinicaltrialsminer.model.Builder;
import org.opencdisc.clinicaltrialsminer.model.ReportType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.List;

public final class Miner {
    private Miner() {}

    public static void mine(Builder options, MinerListener listener) {
        EventDispatcher dispatcher = new EventDispatcher();

        try {
            dispatcher.add(listener);

            List<SearchInput.InputRecord> searchList = options.build();

            String fullPath = "";
            String txtOutputFileName;
            String excelOutputFileName;

            dispatcher.fireTaskStart("", 1, 8);
            for (SearchInput.InputRecord r : searchList) {
                dispatcher.fireTaskIncrement("", 2, 8);
                Utils.downloadData(r);
                String ext = FilenameUtils.getExtension(r.dataPath);
                if (StringUtils.isBlank(ext)) {
                    // no file extention proccedd with default
                    fullPath = r.dataPath + File.separator + r.dataDirName;
                    txtOutputFileName = fullPath + ".txt";
                    excelOutputFileName = fullPath + ".xlsx";
                } else {
                    // file name provided
                    fullPath = FilenameUtils.getFullPath(r.dataPath);
                    String baseName = FilenameUtils.getBaseName(r.dataPath);
                    txtOutputFileName = fullPath + baseName + ".txt";
                    excelOutputFileName = fullPath + baseName + ".xlsx";
                }
                File sourceXMLDir = new File(fullPath);
                FileWriter outputFile = new FileWriter(new File(txtOutputFileName));
                FileOutputStream excelOutputFile = new FileOutputStream(excelOutputFileName);

                ExcelOutput excelOutput = new ExcelOutput(excelOutputFile);
                excelOutput.create();

                int fileCount = 0;
                dispatcher.fireTaskIncrement("", 3, 8);
                ReportType reportType = ReportType.fromString(r.reportType);
                switch (reportType) {
                    case Outcomes:
                        dispatcher.fireTaskIncrement("", 4, 8);
                        OutcomeMeasure.endpointCategoryLoops = 0;
                        OutcomeMeasure outcome = new OutcomeMeasure();
                        outcome.searchName = r.dataDirName;
                        outcome.writeToFile(outputFile, excelOutput, "Header");

                        dispatcher.fireTaskIncrement("", 5, 8);

                        for (File xmlFile : sourceXMLDir.listFiles(new Utils.Filter(".xml"))) {
                            fileCount++;
                            outcome.buildRecord(xmlFile, outputFile, excelOutput, r.storageType, r.categoryFile);
                        }
                        if (r.storageType.equals("DB")) {
                            Utils.finalizeDBConnection(outcome);
                        }                   //TODO: didn't test after moving method to Utils
                        FileUtils.deleteQuietly(r.categoryFile);

                        MinerEvent event = new MinerEvent(MinerEvent.State.Processing, 6, 8, "");
                        event.setSubevent(new MinerEvent.OutcomeEvent(outcome, fileCount));
                        dispatcher.fireTaskIncrement(event);
                        break;
                    case Sites:
                        Site site = new Site();
                        site.writeToFile(outputFile, excelOutput, "Header");
                        for (File xmlFile : sourceXMLDir.listFiles(new Utils.Filter(".xml"))) {
                            fileCount++;
                            site.buildRecord(xmlFile, excelOutput, outputFile, fileCount);
                        }
                        break;
                }
                outputFile.close();
                excelOutput.saveFile();

                MinerEvent event = new MinerEvent(MinerEvent.State.Processing, 7, 8, "");
                event.setSubevent(new MinerEvent.FileCountEvent(fileCount));

                dispatcher.fireTaskIncrement(event);
            }
            dispatcher.fireTaskCompleted(fullPath, 8, 8);
        } catch (Exception ex) {
            throw new RuntimeException(String.format("Miner failed for options %s", options.toString()), ex);
        }
    }
}
