/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {
    public static final short numPad = 10; //for display purposes when formatting numbers to screen
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");

    public static String getTimestamp() {
        return DATE_FORMAT.format(new Date());
    }

    public static NodeList getNodeList(File file, String elementName) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        dbf.setIgnoringComments(true);
        dbf.setIgnoringElementContentWhitespace(true);
        dbf.setNamespaceAware(true);

        DocumentBuilder builder = dbf.newDocumentBuilder();                      //this is the DOM parser
        Document xmlDocument = builder.parse(file);                           //represents the entire XML file
        NodeList nodelist = xmlDocument.getElementsByTagName(elementName); //represents an element tag with everything in it.  no requirement for fully qualified path
        return nodelist;
    }

    public static String getValueByXpath(Element element, String xpathExpression) throws Exception {
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        String value = (String) xpath.evaluate(xpathExpression, element, XPathConstants.STRING);
        return value.replace("\n", " ")    //remove carriage return/newlines
                .replace("\"", "'");   //remove doublequotes which can cause parsing errors for csv and tab delimited.
    }

    //main method which acts like the API to Clinicaltrials.gov.  post search URL and download the xml files
    public static void getTrialZipFile(File rootDirectory, String directoryName, SearchDetails details) throws IOException, ZipException {
        if (rootDirectory.exists()) {
            FileUtils.deleteQuietly(rootDirectory);
        } //delete old results which may not apply anymore
        rootDirectory.mkdir();
        String zipName = rootDirectory.getPath() + "\\search_results.zip";
        File searchResults = new File(zipName);
        ZipFile zipFile = new ZipFile(searchResults);
        FileUtils.copyURLToFile(details.getSearchURL(), searchResults);
        zipFile.extractAll(rootDirectory.getPath());
        FileUtils.deleteQuietly(searchResults);
    }

    //nested class
    public static class Filter implements FileFilter {
        String extension;

        public Filter(String extension) {
            this.extension = extension;
        }

        public boolean accept(File file) {
            if (file.getName().endsWith(extension)) return true;
            else return false;
        }
    }

    public static File sortCSV(File csvFile, int sortColumn) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(csvFile));
        Map<String, List<String>> map = new TreeMap<String, List<String>>();

        String line = reader.readLine();          //read header
        while ((line = reader.readLine()) != null) //read all rows
        {
            String key = line.split(",")[sortColumn - 1]; //extract value you want to sort on, column 1 =[0], column 2 = [1], etc..
            List<String> l = map.get(key);       //get the csv rows associated with this key if it already exists
            if (l == null) {
                l = new LinkedList<String>();
                map.put(key, l);                   //create a new entry in map with placeholder for list..  {JRA = [] }
            }
            l.add(line);
        }
        reader.close();
        String sortedFileName = csvFile.getAbsolutePath().replace(".csv", "_sorted.csv"); //don't modify users original file
        FileWriter writer = new FileWriter(sortedFileName);
        writer.write("Endpoint_Search_Term,End_Point_Category\n");
        for (List<String> list : map.values()) //for each unique category
        {
            for (String val : list) {
                writer.write(val);
                writer.write("\n");
            } //write out all the rows associated with it
        }
        writer.close();
        return new File(sortedFileName);
    }

    public static void getDBConnection(OutcomeMeasure outcome, String url, String user, String password) throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, password); //password is case sensitive
        outcome.preparedInsertStmt = conn.prepareStatement(outcome.insertStatement); //create the skeleton insert statement
    }

    public static void finalizeDBConnection(OutcomeMeasure outcome) throws SQLException {
        outcome.preparedInsertStmt.executeBatch(); //insert any leftovers
        outcome.preparedInsertStmt.close();        //close prepared statement.  will create again for different input.
    }

    public static void downloadData(SearchInput.InputRecord r) throws IOException, ZipException //nestedClass is the type.  could make it its own class.
    {
        File sourceXMLDir = new File(r.dataPath + "/" + r.dataDirName);  //r.dataPath set to ("user.dir")+"/reports unless overridden
        SearchDetails searchDetails = new SearchDetails();                        //generic superclass variable. could get rid of Expert and Advanced Search if necessary
        searchDetails = new ExpertSearch(r.searchString);            //returns normalized URL with "&studyxml=true" suffix
        Utils.getTrialZipFile(sourceXMLDir, r.dataDirName, searchDetails);            //main line of code which acts like the API to Clinicaltrials.gov.  post search URL and download the xml files
    }

    public static void showStats(OutcomeMeasure outcome, //passed in only for showing metrics. TODO, make logic for report specific stats
                                 int fileCount,
                                 long startTime) {
        int rpad = 23; //cosmetic variable
        long endTime = System.currentTimeMillis();
        long seconds = (endTime - startTime) / 1000;
        System.out.println();
        System.out.println(StringUtils.rightPad("Trials (.xml files):", rpad) + StringUtils.leftPad(fileCount + "", Utils.numPad));
        outcome.showAndResetMetrics();
        System.out.println(StringUtils.rightPad("Categorized:", rpad) + StringUtils.leftPad(OutcomeMeasure.categorized + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Uncategorized", rpad) + StringUtils.leftPad(OutcomeMeasure.uncategorized + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Outcomes x Cat checks: ", rpad) + StringUtils.leftPad(OutcomeMeasure.endpointCategoryLoops + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Total seconds:", rpad) + StringUtils.leftPad(seconds + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("=", 100, "="));
    }
}
