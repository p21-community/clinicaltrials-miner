/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.util.List;

//This single object will be reused for each Trial Outcome-measure XML element.  The counts will be cumulative until reset
public class OutcomeMeasure {
    //element tags in ClinicalTrials.gov
    public static final String CLIN_STUDY_TAG = "clinical_study";
    public static final String CONDITION_TAG = "condition";
    public static final String INTERVENTION_TAG = "intervention_name";
    public static final String PRIMARY_OUTCOME_TAG = "primary_outcome";
    public static final String SECONDARY_OUTCOME_TAG = "secondary_outcome";

    public static int endpointCategoryLoops = 0; //outcome measure = clinical endpoint
    public static int categorized = 0;
    public static int uncategorized = 0;
    private static int batchSize = 100;
    private int primaryCount = 0;
    private int secondaryCount = 0;
    private int noOutcomeCount = 0;
    private int universityRunTrials = 0;
    private int seq = 0;
    // http://java67.blogspot.com/2012/12/difference-between-array-vs-arraylist-java.html
    //val[0], Label[1]			    , Xpath[2] (starting context will be clinical_study/
    private String rank[] = new String[]{"", "RANK", "@rank"};
    private String categorization[] = new String[]{"", "Categorization", ""};
    private String measure[] = new String[]{"", "End_Point_Title", "measure"};
    private String timeFrame[] = new String[]{"", "End_Point_TimeFrame", "time_frame"};
    private String safetyIssue[] = new String[]{"", "Saftey_Issue_YN", "safety_issue"};
    private String measureDesc[] = new String[]{"", "End_Point_Details", "description"};
    private String type[] = new String[]{"", "End_Point_Type", ""};
    private String coreOrExt[] = new String[]{"", "Core_Ext_Other_Study", ""};
    private String nctId[] = new String[]{"", "Study_ID", "id_info/nct_id[1]"};
    private String officialTitle[] = new String[]{"", "Official_Title", "official_title[1]"};
    private String sponsor[] = new String[]{"", "Sponsor_Name", "sponsors/lead_sponsor[1]/agency"}; //should agency also have [1]
    private String source[] = new String[]{"", "Source", "source[1]"};
    private String phase[] = new String[]{"", "Phase", "phase[1]"};
    private String studyType[] = new String[]{"", "Study_Type", "study_type[1]"};
    private String dataSource[] = new String[]{"", "Data_Source", ""};
    private String conditions[] = new String[]{"", "Conditions", ""};
    private String interventions[] = new String[]{"", "Intervention", ""};
    private String collaborator[] = new String[]{"", "Collaborator", "sponsors/collaborator[1]/agency"}; //should agency also have [1]
    private String briefSummary[] = new String[]{"", "Brief_Summary", "brief_summary[1]"};
    private String url[] = new String[]{"", "URL", "required_header/url[1]"};
    public String searchURL[] = new String[]{"", "SEARCH_URL", ""};
    private String responsibleParty[] = new String[]{"", "Responsible_Party", ""};
    public String searchName;


    public PreparedStatement preparedInsertStmt;

    public String insertStatement = "INSERT INTO outcomes("
            + "     searchname"   //hardcoded column name
            + "," + categorization[1]
            + "," + measure[1]
            + "," + timeFrame[1]
            + "," + safetyIssue[1]
            + "," + measureDesc[1]
            + "," + type[1]
            + "," + coreOrExt[1]
            + "," + nctId[1]
            + "," + officialTitle[1]
            + "," + sponsor[1]
            //+ "," + source[1]
            + "," + phase[1]
            + "," + studyType[1]
            + "," + dataSource[1]
            + "," + conditions[1]
            + "," + interventions[1]
            + "," + collaborator[1]
            + "," + briefSummary[1]
            + "," + url[1]
            + "," + searchURL[1]
            + "," + responsibleParty[1]
            + ",seq"
            + ") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";    //22 question marks
    //01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
    //if you get java.sql.SQLException: Missing IN or OUT parameter at index:: NN, check your question marks

    public void writeToDB() {
        try {
            preparedInsertStmt.setString(1, searchName); //passed in from input file
            preparedInsertStmt.setString(2, categorization[0]);
            preparedInsertStmt.setString(3, measure[0]);
            preparedInsertStmt.setString(4, timeFrame[0]);
            preparedInsertStmt.setString(5, safetyIssue[0]);
            preparedInsertStmt.setString(6, measureDesc[0]);
            preparedInsertStmt.setString(7, type[0]);
            preparedInsertStmt.setString(8, coreOrExt[0]);
            preparedInsertStmt.setString(9, nctId[0]);
            preparedInsertStmt.setString(10, officialTitle[0]);
            preparedInsertStmt.setString(11, sponsor[0]);
            //preparedInsertStmt.setString(	12	,	source[0]			); //did not create a DB column for this.
            preparedInsertStmt.setString(12, phase[0]);
            preparedInsertStmt.setString(13, studyType[0]);
            preparedInsertStmt.setString(14, dataSource[0]);
            preparedInsertStmt.setString(15, conditions[0]);
            preparedInsertStmt.setString(18, interventions[0]);
            preparedInsertStmt.setString(19, collaborator[0]);
            preparedInsertStmt.setString(20, briefSummary[0]); //unless i declare as CLOB OR LONG, i need this to prevent oracle error
            preparedInsertStmt.setString(21, url[0]);
            preparedInsertStmt.setString(22, searchURL[0]);
            preparedInsertStmt.setString(23, responsibleParty[0]);
            preparedInsertStmt.setInt(24, seq);
            preparedInsertStmt.addBatch();

            //preparedInsertStmt.executeUpdate();
            if (seq % batchSize == 0) //insert every N outcomes
            {
                preparedInsertStmt.executeBatch();

            }
        } catch (Exception e) {
            System.out.println("\nSEQ:" + seq + ": " + e.getMessage() + insertStatement);
            showRecord();
        }
    }

    public void writeToFile(FileWriter outputFile, ExcelOutput excelOutput, String outputType) throws IOException //all values should be encolsed with " " to avoid having to escape or replace delimiters in each column
    {
        String D = "\t";                                  //tab Delimiter: auto recognized by Microsoft EXCEL.  TODO create actual excel file with Apache POI
        String COLUMN_FORMAT = "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s\n";
        String header = String.format(COLUMN_FORMAT, "SEQ", categorization[1], measure[1], timeFrame[1], safetyIssue[1], measureDesc[1], type[1], coreOrExt[1], nctId[1], officialTitle[1], sponsor[1], phase[1], studyType[1], dataSource[1], conditions[1], interventions[1]);
        String record = String.format(COLUMN_FORMAT, seq, categorization[0], measure[0], timeFrame[0], safetyIssue[0], measureDesc[0], type[0], coreOrExt[0], nctId[0], officialTitle[0], sponsor[0], phase[0], studyType[0], dataSource[0], conditions[0], interventions[0]);

        if (outputType.equals("Header")) {/*Trials will be printed during outcome.WriteOutcomes (if any)*/
            outputFile.write(header);
            System.out.print("Trials with no POMs: ");
            excelOutput.insertHeader("SEQ", categorization[1], measure[1], timeFrame[1], safetyIssue[1], measureDesc[1], type[1], coreOrExt[1], nctId[1], officialTitle[1], sponsor[1], phase[1], studyType[1], dataSource[1], conditions[1], interventions[1]);
        } else if (outputType.equals("Record")) {
            outputFile.write(record);
            excelOutput.insertRow(Integer.toString(seq), categorization[0], measure[0], timeFrame[0], safetyIssue[0], measureDesc[0], type[0], coreOrExt[0], nctId[0], officialTitle[0], sponsor[0], phase[0], studyType[0], dataSource[0], conditions[0], interventions[0]);
        }
    }

    public void writeOutcomes(File xmlFile,
                              FileWriter outputFile,
                              ExcelOutput excelOutput,
                              String outcomeType,
                              String storageType,
                              File categoryFile) throws Exception {
        type[0] = outcomeType;
        NodeList outcomes = null;
        String outcomeTypeTag = "";

        //need to check once, just to see which tag to use.
        if (type[0].equals("Primary")) {
            outcomeTypeTag = PRIMARY_OUTCOME_TAG;
        } else {
            outcomeTypeTag = SECONDARY_OUTCOME_TAG;
        }
        ;

        outcomes = Utils.getNodeList(xmlFile, outcomeTypeTag);

        int i = 0;
        for (i = 0; i < outcomes.getLength(); ++i) {
            if (type[0].equals("Primary")) {
                primaryCount++;
            }  //need to increment multiple times, since many Primary or Secondary outcomes can exist for a Trial
            else {
                secondaryCount++;
            }
            ;

            timeFrame[0] = Utils.getValueByXpath((Element) outcomes.item(i), timeFrame[2]);
            safetyIssue[0] = Utils.getValueByXpath((Element) outcomes.item(i), safetyIssue[2]);
            measureDesc[0] = Utils.getValueByXpath((Element) outcomes.item(i), measureDesc[2]).replace("\t", " ");
            measure[0] = Utils.getValueByXpath((Element) outcomes.item(i), measure[2]).replace("\t", " ");
            categorization[0] = ""; //reset category

            assignUserCategories(categoryFile);

            if (StringUtils.containsIgnoreCase(measure[0], "core study")) {
                coreOrExt[0] = "CORE";
            } else if (StringUtils.containsIgnoreCase(measure[0], "extension study")) {
                coreOrExt[0] = "EXTENSION";
            }
            seq++;
            if (storageType.equalsIgnoreCase("file")) {
                writeToFile(outputFile, excelOutput, "Record");
            } else if (storageType.equalsIgnoreCase("DB")) {
                writeToDB();
            } //main.java gets the DB Connection and preparedStatement (insert statement)
        }

        if (i == 0 && outcomeType.equals("Primary")) {
            noOutcomeCount++;
            System.out.print(nctId[0] + "|");
        } //after coming out of the for loop, if you never iterated, then no primary outcomes...probably a data entry error or missing data in clinicalTrials.gov
    }

    public void assignUserCategories(File categoryFile) throws IOException {
        if (categoryFile.exists()) {
            CSVReader csvReader = new CSVReader(new FileReader(categoryFile));
            List<String[]> csvCatRecords = csvReader.readAll(); //String[] since each row is a comma delimited string

			/*for each record (a string array of 2 values representing the 2 column excel file)...
             * check measure[0] against all possible mappings to assign DISPLAY_CATEGORY
			 * searchTerm[0] = SEARCH_TERM
			 * searchTerm[1] = DISPLAY_CATEGORY */
            int j = 1;
            for (String[] searchTerm : csvCatRecords) {
                if (j != 1) {
                    OutcomeMeasure.endpointCategoryLoops++;  //static class variable shared for all outcome instances during a given run

                    if (measure[0].toUpperCase().contains(searchTerm[0].toUpperCase())  //does my outcome-measure match any row in the mapping table
                            && !categorization[0].contains(searchTerm[1]))             //Don't add if the display category was already added.
                    {
                        if (categorization[0].equals("")) {
                            categorization[0] = categorization[0].concat(searchTerm[1]);
                        } else {
                            categorization[0] = categorization[0].concat(" |" + searchTerm[1]);
                        }
                    }
                }
                j++;
            }
            csvReader.close();
            if (categorization[0].equals("")) {
                uncategorized++;
            } else {
                categorized++;
            }

        }
    }

    public void setDimensions(File xmlFile) throws Exception {
        Element clinicalStudy = (Element) Utils.getNodeList(xmlFile, CLIN_STUDY_TAG).item(0); //there is only 1 element
        rank[0] = Utils.getValueByXpath(clinicalStudy, rank[2]);
        url[0] = Utils.getValueByXpath(clinicalStudy, url[2]);
        nctId[0] = Utils.getValueByXpath(clinicalStudy, nctId[2]);
        officialTitle[0] = Utils.getValueByXpath(clinicalStudy, officialTitle[2]).replace("\"", ""); //one study NCT00365846 has begins with ", but doesn't end with it.  causes issues in excel
        briefSummary[0] = Utils.getValueByXpath(clinicalStudy, briefSummary[2]).replace("\"", "");
        phase[0] = Utils.getValueByXpath(clinicalStudy, phase[2]);
        studyType[0] = Utils.getValueByXpath(clinicalStudy, studyType[2]);
        sponsor[0] = Utils.getValueByXpath(clinicalStudy, sponsor[2]);
        collaborator[0] = Utils.getValueByXpath(clinicalStudy, collaborator[2]);
        source[0] = Utils.getValueByXpath(clinicalStudy, source[2]);
        if (!source.equals(sponsor[0])) {
            sponsor[0] = sponsor[0] + "-" + source[0];
        } //in some cases, sponsor and source may be different
        if (StringUtils.containsIgnoreCase(sponsor[0] + "-" + source[0], "university")) {
            universityRunTrials++;
        } //keep track of university run trials
    }

    public void setConditions(File xmlFile) throws Exception {
        NodeList allConditions = Utils.getNodeList(xmlFile, CONDITION_TAG);
        for (int i = 0; i < allConditions.getLength(); ++i) {
            if (i == 0) {
                this.conditions[0] = allConditions.item(i).getTextContent();
            } else {
                this.conditions[0] = this.conditions[0] + "|" + allConditions.item(i).getTextContent();
            }
        }
    }

    public void setInterventions(File xmlFile) throws Exception {
        String priorIntervention = "";
        NodeList allInterventions = Utils.getNodeList(xmlFile, INTERVENTION_TAG);
        for (int i = 0; i < allInterventions.getLength(); ++i) {
            if (!(allInterventions.item(i).getTextContent()).equals(priorIntervention)) {
                if (i == 0) {
                    this.interventions[0] = allInterventions.item(i).getTextContent();
                } else {
                    this.interventions[0] = this.interventions[0] + "|" + allInterventions.item(i).getTextContent();
                }
            }
            priorIntervention = allInterventions.item(i).getTextContent();
        }
    }

    public void setSearchUrl(URL url) {
        searchURL[0] = url.toString().replace("&studyxml=true", "");
    }

    public void resetConditionsAndInterventions() {
        conditions[0] = "";
        interventions[0] = "";
    }

    //should this method be moved to Utils ?
    public void showAndResetMetrics() // call this arbitrarily.  Normally after an instance of a ClinicalTrial search result (downloaded zip file) which will contains many trials
    {
        int rpad = 23;
        System.out.println(StringUtils.rightPad("Trials with no POMs:", rpad) + StringUtils.leftPad(noOutcomeCount + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("University Run trials:", rpad) + StringUtils.leftPad(universityRunTrials + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Primary   Outcomes:", rpad) + StringUtils.leftPad(primaryCount + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Secondary Outcomes:", rpad) + StringUtils.leftPad(secondaryCount + "", Utils.numPad));
        System.out.println(StringUtils.rightPad("Total*    Outcomes:", rpad) + StringUtils.leftPad(primaryCount + secondaryCount + "", Utils.numPad));
        primaryCount = 0;
        secondaryCount = 0;
    }

    public void buildRecord(File xmlFile,
                            FileWriter outputFile,
                            ExcelOutput excelOutput,
                            String storageType,
                            File categoryFile) //2 column csv file with SEARCH_TERM and DISPLAY_CATEGORY
            throws Exception {
        resetConditionsAndInterventions();
        setDimensions(xmlFile);
        setConditions(xmlFile);
        setInterventions(xmlFile);
        writeOutcomes(xmlFile, outputFile, excelOutput, "Primary", storageType, categoryFile);
        writeOutcomes(xmlFile, outputFile, excelOutput, "Secondary", storageType, categoryFile);
    }

    public void showRecord() {
        System.out.println(
                //categorization[1] 	+ ":" +categorization[0] 	+ "\n" +
                //measure[1] 			+ ":" +measure[0] 			+ "\n" +
                //timeFrame[1] 		    + ":" +timeFrame[0] 		+ "\n" +
                //safetyIssue[1] 		+ ":" +safetyIssue[0] 		+ "\n" +
                //measureDesc[1] 		+ ":" +measureDesc[0] 		+ "\n" +
                //type[1] 			    + ":" +type[0] 				+ "\n" +
                //coreOrExt[1] 		    + ":" +coreOrExt[0] 		+ "\n" +
                nctId[1] + ":" + nctId[0] + "\n" +
                        //officialTitle[1] 	    + ":" +officialTitle[0] 	+ "\n" +
                        //sponsor[1] 			+ ":" +sponsor[0] 			+ "\n" +
                        //source[1] 			+ ":" +source[0] 			+ "\n" +
                        //phase[1] 			    + ":" +phase[0] 			+ "\n" +
                        //studyType[1] 		    + ":" +studyType[0] 		+ "\n" +
                        //dataSource[1]     	+ ":" +dataSource[0]     	+ "\n" +
                        //conditions[1] 		+ ":" +conditions[0] 		+ "\n" +
                        //keyIntervTrade[1] 	+ ":" +keyIntervTrade[0] 	+ "\n" +
                        //keyIntervGeneric[1]   + ":" +keyIntervGeneric[0] 	+ "\n" +
                        //interventions[1] 	    + ":" +interventions[0] 	+ "\n" +
                        //collaborator[1] 	    + ":" +collaborator[0] 		+ "\n" +
                        briefSummary[1] + ":" + briefSummary[0] + "\n" +
                        //url[1] 				+ ":" +url[0] 				+ "\n" +
                        searchURL[1] + ":" + searchURL[0]);
    }

}
