/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Test {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        String test = "";
        String a = "";

        test = !a.equals("") && a != null ? a : System.getProperty("user.dir");

        System.out.println(test);

        File file = new File("text.txt");

        System.out.println(file.getAbsolutePath());
        System.out.println(file.getCanonicalPath());
        System.out.println(System.getProperty("user.dir"));

        List<String> list1 = Arrays.asList(new String[4]);
        List<String> list2 = Arrays.asList(new String[]{"a", "b"});
        List<String> list3 = Arrays.asList("mike", "digian", "p21");

        System.out.println("list1 " + list1);
        System.out.println("list2 " + list2);
        System.out.println("list3 " + list3);
        System.out.println(list3.get(0));

    }

}
