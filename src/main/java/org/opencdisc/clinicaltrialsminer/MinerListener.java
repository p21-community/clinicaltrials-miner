/*
 * Copyright © 2008-2015 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

public interface MinerListener {
    /**
     * @param event  a <code>MinerEvent</code> with detailed information about the
     *     current progress
     */
    public void generatorProgressUpdated(MinerEvent event);

    /***
     * @param event a blank <code>MinerEvent</code> object
     */
    public void generatorStarted(MinerEvent event);

    /**
     * @param event a blank <code>MinerEvent</code> object
     */
    public void generatorStopped(MinerEvent event);
}
