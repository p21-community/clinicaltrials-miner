/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

//Create interface or abstract class which OutcomeMeasure and Sites implement

/* Example of elements from downloaded clinicaltrials.gov xml file
  <clinical_study>
    <responsible_party>
        <responsible_party_type>  Principal Investigator       </responsible_party_type>
        <investigator_affiliation>Johns Hopkins University     </investigator_affiliation>
        <investigator_full_name>  Thomas Aversano              </investigator_full_name>
        <investigator_title>      Principal Investigator, CPORT</investigator_title>
    </responsible_party>
    <!-- MANY LOCATIONS PER TRIAL XML DOC -->
    <location>
        <status>Recruiting</status>
        <facility>
        	<name>Meritus Medical Center</name>
        	<address>
        		<city>Hagerstown</city><state>Maryland</state><zip>21742</zip><country>United States</country>
        	</address>
        </facility>
        <investigator>  <last_name>Robert Marshall, MD</last_name><role>Principal Investigator</role></investigator>
        <contact>       <last_name>Barbara Elmore, RN </last_name><phone>301-790-8227</phone><email>barbara.elmore@meritushealth.com</email></contact>
        <contact_backup><last_name>Betty Myers, RN    </last_name><phone>301-790-8458</phone><email>Betty.Myers@meritushealth.com   </email></contact_backup>
    </location>
 */
public class Site {
    //element tags in ClinicalTrials.gov
    public static final String CLIN_STUDY_TAG = "clinical_study";  //TODO: refactor out
    public static final String RESP_PARTY = "responsible_party";

    public static int siteCount = 0;
    private int cumulativeStudyCount = 0;


    //Map sites<string

    private int seq = 0;
    //value[0], File Label[1]			      , Xpath[2]
    private String[] nct_id = new String[]{"", "NCT_ID", "id_info/nct_id[1]"}; //TODO: factor out for all extracts
    private String org_study_id[] = new String[]{"", "ORG_STUDY_ID", "id_info/org_study_id[1]"}; //TODO: factor out for all extracts
    private String responsible_party_type[] = new String[]{"", "RESPONSIBLE_PARTY_TYPE", RESP_PARTY + "/responsible_party_type  [1]"}; //the [1] is the first element in the xpath
    private String investigator_affiliation[] = new String[]{"", "INVESTIGATOR_AFFILIATION", RESP_PARTY + "/investigator_affiliation[1]"};
    private String investigator_full_name[] = new String[]{"", "INVESTIGATOR_FULL_NAME", RESP_PARTY + "/investigator_full_name  [1]"};
    private String investigator_title[] = new String[]{"", "INVESTIGATOR_TITLE", RESP_PARTY + "/investigator_title      [1]"};
    //starting context is /clinical_study/location[i]
    private String facility_status[] = new String[]{"", "FACILITY_STATUS", "status"};
    private String facility_name[] = new String[]{"", "FACILITY_NAME", "facility/name"};
    private String facility_city[] = new String[]{"", "FACILITY_CITY", "facility/address/city"};
    private String facility_state[] = new String[]{"", "FACILITY_STATE", "facility/address/state"};
    private String facility_zip[] = new String[]{"", "FACILITY_ZIP", "facility/address/zip"};
    private String facility_country[] = new String[]{"", "FACILITY_COUNTRY", "facility/address/country"};
    private String investigator_name[] = new String[]{"", "INVESTIGATOR_NAME", "investigator/last_name"};
    private String investigator_role[] = new String[]{"", "INVESTIGATOR_ROLE", "investigator/role"};
    private String contact_name[] = new String[]{"", "CONTACT_NAME", "contact/last_name"};
    private String contact_phone[] = new String[]{"", "CONTACT_PHONE", "contact/phone"};
    private String contact_email[] = new String[]{"", "CONTACT_EMAIL", "contact/email"};
    private String contact_backup_name[] = new String[]{"", "CONTACT_BACKUP_NAME", "contact_backup/last_name"};
    private String contact_backup_phone[] = new String[]{"", "CONTACT_BACKUP_PHONE", "contact_backup/phone"};
    private String contact_backup_email[] = new String[]{"", "CONTACT_BACKUP_EMAIL", "contact_backup/email"};


    public void writeToFile(FileWriter outputFile, ExcelOutput excelOutput, String outputType) throws IOException {
        //all values should be encolsed with " " to avoid having to escape or replace delimiters in each column
        String D = "\t";             //tab Delimiter: auto recognized by Microsoft EXCEL
        String COLUMN_FORMAT = "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s" + D + "%s\n";
        String header = String.format(COLUMN_FORMAT, "SEQ", nct_id[1], org_study_id[1], responsible_party_type[1], investigator_affiliation[1], investigator_full_name[1], investigator_title[1], facility_status[1], facility_name[1], facility_city[1], facility_state[1], facility_zip[1], facility_country[1], investigator_name[1], investigator_role[1], contact_name[1], contact_phone[1], contact_email[1], contact_backup_name[1], contact_backup_phone[1], contact_backup_email[1]);
        String record = String.format(COLUMN_FORMAT, seq, nct_id[0], org_study_id[0], responsible_party_type[0], investigator_affiliation[0], investigator_full_name[0], investigator_title[0], facility_status[0], facility_name[0], facility_city[0], facility_state[0], facility_zip[0], facility_country[0], investigator_name[0], investigator_role[0], contact_name[0], contact_phone[0], contact_email[0], contact_backup_name[0], contact_backup_phone[0], contact_backup_email[0]);

        if (outputType.equals("Header")) {
            outputFile.write(header);
            excelOutput.insertHeader("SEQ", nct_id[1], org_study_id[1], responsible_party_type[1], investigator_affiliation[1], investigator_full_name[1], investigator_title[1], facility_status[1], facility_name[1], facility_city[1], facility_state[1], facility_zip[1], facility_country[1], investigator_name[1], investigator_role[1], contact_name[1], contact_phone[1], contact_email[1], contact_backup_name[1], contact_backup_phone[1], contact_backup_email[1]);
        } else if (outputType.equals("Record")) {
            outputFile.write(record);
            excelOutput.insertRow(Integer.toString(seq), nct_id[0], org_study_id[0], responsible_party_type[0], investigator_affiliation[0], investigator_full_name[0], investigator_title[0], facility_status[0], facility_name[0], facility_city[0], facility_state[0], facility_zip[0], facility_country[0], investigator_name[0], investigator_role[0], contact_name[0], contact_phone[0], contact_email[0], contact_backup_name[0], contact_backup_phone[0], contact_backup_email[0]);
        }
    }


    public void setDimensions(File xmlFile) throws Exception {
        Element clinicalStudy = (Element) Utils.getNodeList(xmlFile, CLIN_STUDY_TAG).item(0); //there is only 1 element, thus item(0). factor this out for all extracts (sites,outcomes,etc..)

        nct_id[0] = Utils.getValueByXpath(clinicalStudy, nct_id[2]);
        org_study_id[0] = Utils.getValueByXpath(clinicalStudy, org_study_id[2]);
        responsible_party_type[0] = Utils.getValueByXpath(clinicalStudy, responsible_party_type[2]).replace("\"", "");
        investigator_affiliation[0] = Utils.getValueByXpath(clinicalStudy, investigator_affiliation[2]).replace("\"", "");
        investigator_full_name[0] = Utils.getValueByXpath(clinicalStudy, investigator_full_name[2]).replace("\"", "");
        investigator_title[0] = Utils.getValueByXpath(clinicalStudy, investigator_title[2]).replace("\"", "");
    }

    public void writeSiteLocations(File xmlFile,
                                   ExcelOutput excelOutput,
                                   FileWriter outputFile) throws Exception {
        NodeList siteLocations = Utils.getNodeList(xmlFile, "location"); //get all location elements 1 to n
        for (int i = 0; i < siteLocations.getLength(); ++i) {
            seq++;
            siteCount++;
            facility_status[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_status[2]);// location[i]/status
            facility_name[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_name[2]);// location[i]/facility/name
            facility_city[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_city[2]);// location[i]/facility/address/city
            facility_state[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_state[2]);// location[i]/facility/address/state
            facility_zip[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_zip[2]);// location[i]/facility/address/zip
            facility_country[0] = Utils.getValueByXpath((Element) siteLocations.item(i), facility_country[2]);// location[i]/facility/address/country
            investigator_name[0] = Utils.getValueByXpath((Element) siteLocations.item(i), investigator_name[2]);// location[i]/investigator/last_name
            investigator_role[0] = Utils.getValueByXpath((Element) siteLocations.item(i), investigator_role[2]);// location[i]/investigator/role
            contact_name[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_name[2]);// location[i]/contact/last_name
            contact_phone[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_phone[2]);// location[i]/contact/phone
            contact_email[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_email[2]);// location[i]/contact/email
            contact_backup_name[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_backup_name[2]);// location[i]/contact_backup/last_name
            contact_backup_phone[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_backup_phone[2]);// location[i]/contact_backup/phone
            contact_backup_email[0] = Utils.getValueByXpath((Element) siteLocations.item(i), contact_backup_email[2]);// location[i]/contact_backup/email

            writeToFile(outputFile, excelOutput, "Record");  //showRecord(); //debug each record as name-value pairs

            if (siteCount % 5000 == 0) {
                System.out.println("Sites processed so far: " + StringUtils.leftPad(String.valueOf(siteCount), 10) + " on Study: " + this.cumulativeStudyCount);
            }
        }
    }

    public void buildRecord(File xmlFile,
                            ExcelOutput excelOutput,
                            FileWriter outputFile,
                            int cumulativeStudyCount) throws Exception {
        this.cumulativeStudyCount = cumulativeStudyCount;
        setDimensions(xmlFile);
        writeSiteLocations(xmlFile, excelOutput, outputFile);
    }

    public void showAndResetMetrics() {

    }

    public void showRecord() //debug method
    {
        int pad = 30;
        System.out.println( //label : value
                StringUtils.rightPad("seq" + ":", pad) + seq + "\n" +
                        StringUtils.rightPad(nct_id[1] + ":", pad) + nct_id[0] + "\n" +
                        StringUtils.rightPad(org_study_id[1] + ":", pad) + org_study_id[0] + "\n" +
                        StringUtils.rightPad(responsible_party_type[1] + ":", pad) + responsible_party_type[0] + "\n" +
                        StringUtils.rightPad(investigator_affiliation[1] + ":", pad) + investigator_affiliation[0] + "\n" +
                        StringUtils.rightPad(investigator_full_name[1] + ":", pad) + investigator_full_name[0] + "\n" +
                        StringUtils.rightPad(investigator_title[1] + ":", pad) + investigator_title[0] + "\n" +
                        StringUtils.rightPad(facility_status[1] + ":", pad) + facility_status[0] + "\n" +
                        StringUtils.rightPad(facility_name[1] + ":", pad) + facility_name[0] + "\n" +
                        StringUtils.rightPad(facility_city[1] + ":", pad) + facility_city[0] + "\n" +
                        StringUtils.rightPad(facility_state[1] + ":", pad) + facility_state[0] + "\n" +
                        StringUtils.rightPad(facility_zip[1] + ":", pad) + facility_zip[0] + "\n" +
                        StringUtils.rightPad(facility_country[1] + ":", pad) + facility_country[0] + "\n" +
                        StringUtils.rightPad(investigator_name[1] + ":", pad) + investigator_name[0] + "\n" +
                        StringUtils.rightPad(investigator_role[1] + ":", pad) + investigator_role[0] + "\n" +
                        StringUtils.rightPad(contact_name[1] + ":", pad) + contact_name[0] + "\n" +
                        StringUtils.rightPad(contact_phone[1] + ":", pad) + contact_phone[0] + "\n" +
                        StringUtils.rightPad(contact_email[1] + ":", pad) + contact_email[0] + "\n" +
                        StringUtils.rightPad(contact_backup_name[1] + ":", pad) + contact_backup_name[0] + "\n" +
                        StringUtils.rightPad(contact_backup_phone[1] + ":", pad) + contact_backup_phone[0] + "\n" +
                        StringUtils.rightPad(contact_backup_email[1] + ":", pad) + contact_backup_email[0] + "\n" +
                        ""
        );
    }


}
