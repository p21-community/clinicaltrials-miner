/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import org.apache.commons.lang3.StringUtils;
import org.opencdisc.clinicaltrialsminer.model.Builder;

public class Main {
    private static long startTime = System.currentTimeMillis();
    private static String db_URL = "jdbc:oracle:thin:@//localhost/XE", USER = "bridg", PASSWORD = "bridg"; //password is case sensitive

    public static void main(final String[] args) {
        final Builder options = new Builder();
        options.setInputFile(args[0]);
        Miner.mine(options, new MinerListener() {
            @Override
            public void generatorProgressUpdated(MinerEvent event) {
                if (event.getSubevent() != null) {
                    if (event.getSubevent() instanceof MinerEvent.OutcomeEvent) {
                        MinerEvent.OutcomeEvent outcomeEvent = (MinerEvent.OutcomeEvent) event.getSubevent();
                        Utils.showStats(outcomeEvent.getOutcome(),
                                outcomeEvent.getFileCount(), startTime);
                    } else {
                        MinerEvent.FileCountEvent fileCountEvent = (MinerEvent.FileCountEvent) event.getSubevent();
                        System.out.println("Sites processed total : " + StringUtils.leftPad(String.valueOf(Site.siteCount), 10));
                        long seconds = (System.currentTimeMillis() - startTime) / 1000;
                        System.out.println(StringUtils.rightPad("Trials (.xml files):", 20) + StringUtils.leftPad(fileCountEvent.getFileCount() + "", Utils.numPad));
                        System.out.println(StringUtils.rightPad("Total seconds:", 20) + StringUtils.leftPad(seconds + "", Utils.numPad));
                    }
                }
            }

            @Override
            public void generatorStarted(MinerEvent event) {

            }

            @Override
            public void generatorStopped(MinerEvent event) {
                System.out.println("FINISHED");
            }
        });
    }
}
