/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Piotr on 10/28/2014.
 */

public class ExcelOutput {

    private static final XSSFColor HEADER_FONT_COLOR = new XSSFColor(new Color(79, 98, 40));
    private static final XSSFColor HEADER_BACKROUND_COLOR = new XSSFColor(new Color(194, 214, 154));
    private HashMap<Integer, Integer> columnSizes = new HashMap<>();
    private final FileOutputStream fileOut;
    private SXSSFWorkbook workBook;
    private Sheet sheet;
    private int rowNumber = 1;

    public ExcelOutput(FileOutputStream fileOut) {
        this.fileOut = fileOut;
    }

    public void create() {
        workBook = new SXSSFWorkbook();
        sheet = workBook.createSheet();
    }

    // TODO: The parameters should probably something more strict
    public void insertHeader(String... data) {

        org.apache.poi.ss.usermodel.Font font = workBook.createFont();
        font.setFontHeightInPoints((short) 11);
        font.setColor(HEADER_FONT_COLOR.getIndexed());
        font.setFontName("Arial");

        XSSFCellStyle styleHeader = (XSSFCellStyle) workBook.createCellStyle();
        styleHeader.setFont(font);

        styleHeader.setFillForegroundColor(HEADER_BACKROUND_COLOR);
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < data.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(data[i]);
            cell.setCellStyle(styleHeader);
            setColumnSize(i, data[i].length());
        }
    }

    // TODO: The parameters should probably something more strict
    public void insertRow(String... data) {
        Row row = sheet.createRow(rowNumber++);

        for (int i = 0; i < data.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(data[i]);
            setColumnSize(i, data[i].length());
        }
    }

    public void saveFile() throws IOException {

        resizeColumns();
        workBook.write(fileOut);
        fileOut.flush();
        fileOut.close();
        workBook.dispose();
    }

    // Try to auto size columns as best as you can without making columns too wide. Found that each character is approximately 600 units
    public void setColumnSize(int row, int size) {
        size = (size * 600) > 6000 ? 6000 : (size * 600);

        if (columnSizes.get(row) == null) {
            columnSizes.put(row, size);
        } else if (size > columnSizes.get(row)) {
            columnSizes.put(row, size);
        }
    }

    public void resizeColumns() {
        for (int key : columnSizes.keySet()) {
            sheet.setColumnWidth(key, columnSizes.get(key));
        }
    }
}
