/*
 * Copyright © 2008-2015 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.clinicaltrialsminer;

import java.util.LinkedHashSet;
import java.util.Set;

public class EventDispatcher {
    private final Set<MinerListener> listeners =
        new LinkedHashSet<MinerListener>();

    public synchronized void add(MinerListener listener) {
        this.listeners.add(listener);
    }

    public synchronized void clear() {
        this.listeners.clear();
    }
//
//    public void fireParsing(String configurationPath) {
//        this.dispatchUpdated(new ConverterEvent(State.Parsing, configurationPath));
//    }

    public void fireTaskStart(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new MinerEvent(MinerEvent.State.Processing, currentTask,
                totalTasks, taskName));
    }

    public void fireTaskIncrement(String taskName, long currentTask, long totalTasks) {
        MinerEvent event = new MinerEvent(MinerEvent.State.Processing, currentTask,
                totalTasks, taskName);

//        event.setSubevent(new ConverterEvent(State.Processing, currentIncrement));

        this.dispatchUpdated(event);
    }

    public void fireTaskIncrement(MinerEvent event) {
        this.dispatchUpdated(event);
    }

    public void fireTaskCompleted(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new MinerEvent(MinerEvent.State.Completed, currentTask, totalTasks,
                taskName));
    }


    public void fireGenerating(String resultPath) {
        this.dispatchUpdated(new MinerEvent(MinerEvent.State.Generating, resultPath));
    }

    public void fireStarted() {
        this.dispatchStarted(new MinerEvent());
    }

    public void fireStopped() {
        this.dispatchStopped(new MinerEvent());
    }

    public synchronized boolean remove(MinerListener listener) {
        return this.listeners.remove(listener);
    }

    private synchronized Set<MinerListener> copyListeners() {
        return new LinkedHashSet<MinerListener>(this.listeners);
    }

    private void dispatchUpdated(MinerEvent event) {
        Set<MinerListener> listeners = this.copyListeners();

        for (MinerListener listener : listeners) {
            synchronized (listener) {
                listener.generatorProgressUpdated(event);
            }
        }
    }

    private void dispatchStarted(MinerEvent event) {
        Set<MinerListener> listeners = this.copyListeners();

        for (MinerListener listener : listeners) {
            synchronized (listener) {
                listener.generatorStarted(event);
            }
        }
    }

    private void dispatchStopped(MinerEvent event) {
        Set<MinerListener> listeners = this.copyListeners();

        for (MinerListener listener : listeners) {
            synchronized (listener) {
                listener.generatorStopped(event);
            }
        }
    }
}
