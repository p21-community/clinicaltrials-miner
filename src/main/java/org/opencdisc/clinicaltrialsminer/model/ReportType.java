package org.opencdisc.clinicaltrialsminer.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum ReportType {
    Outcomes("Outcomes"),
    Sites("Sites"),
    UNKNOWN("");

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportType.class);

    private final String value;

    ReportType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ReportType fromString(String type) {
        for (ReportType t: values()) {
            if (t.name().equalsIgnoreCase(type) ||
                    t.value.equalsIgnoreCase(type)) {
                return t;
            }
        }

        LOGGER.warn("Undefined report type {}", type);
        return UNKNOWN;
    }
}
