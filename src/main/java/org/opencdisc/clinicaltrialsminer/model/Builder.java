package org.opencdisc.clinicaltrialsminer.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import org.opencdisc.clinicaltrialsminer.SearchInput;

import java.io.File;
import java.util.List;

public class Builder {
    private String inputFile;
    private File csvFile;
    private String query;
    private ReportType reportType;
    private String filePath;

    public Builder setInputFile(String inputFile) {
        this.inputFile = inputFile;
        return this;
    }

    public Builder setCsvFile(File csvFile) {
        this.csvFile = csvFile;
        return this;
    }

    public Builder setQuery(String query) {
        this.query = query;
        return this;
    }

    public Builder setReportType(ReportType reportType) {
        this.reportType = reportType;
        return this;
    }

    public Builder setReportType(String reportType) {
        this.reportType = ReportType.fromString(reportType);
        return this;
    }


    public Builder setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public List<SearchInput.InputRecord> build() {
        try {
            SearchInput searchInput = new SearchInput();
            List<SearchInput.InputRecord> inputRecords;

            if (!Strings.isNullOrEmpty(this.inputFile)) {
                inputRecords = searchInput.getSearchList(new File(this.inputFile));
            } else {
                inputRecords = searchInput.getQueryList(this.query,
                        this.reportType.getValue(), this.csvFile, this.filePath);
            }

            return inputRecords;
        } catch (Exception e) {
            throw new RuntimeException(String.format("Miner failed for options %s", this.toString()), e);
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("inputFile", inputFile)
                .add("csvFile", csvFile)
                .add("query", query)
                .add("reportType", reportType)
                .add("filePath", filePath)
                .toString();
    }
}
